from datetime import datetime as dt
from sqlalchemy.engine import ResultProxy

from sqlalchemy import (
    MetaData, Table, Column, ForeignKey,
    Integer, String, DateTime,
    join, select)

meta = MetaData()

# Модель пользователя
users = Table(
    'users', meta,
    Column('id', Integer, primary_key=True),
    Column('username', String(200), nullable=False, unique=True),
    Column('password', String(200), nullable=False)
)

# Модель категории постов
category = Table(
    'category', meta,
    Column('id', Integer, primary_key=True),
    Column('name', String(140), unique=True),
)

# Модель постов пользователя
posts = Table(
    'posts', meta,

    Column('id', Integer, primary_key=True),
    Column('name', String(140), unique=True),
    Column('body', String(255)),
    Column('file_name', String(255), unique=True, nullable=True),
    Column('timestamp', DateTime, index=True, default=dt.utcnow),

    Column('user_id',
           Integer,
           ForeignKey('users.id')),

    Column('category_id',
           Integer,
           ForeignKey('category.id'))
)

# Модель избранных постов пользователя
favorite = Table(
    'favorite', meta,

    Column('id', Integer, primary_key=True),
    Column('timestamp', DateTime, index=True, default=dt.utcnow),

    Column('user_id',
           Integer,
           ForeignKey('users.id')),

    Column('post_id',
           Integer,
           ForeignKey('posts.id'))
)


async def db_add_post_to_favorite(conn, post_id: int, user_id: int):
    """
    Добавляет пост с id в избранное для пользователя с id
    :param conn:
    :param post_id:
    :param user_id:
    :return:
    """
    stmt = favorite.insert().values(
        user_id=user_id,
        post_id=post_id
    )
    await conn.execute(stmt)


async def db_delete_post_to_favorite(conn, post_id: int, user_id: int):
    """
    Удаляет пост с id из избранного для пользователя
    :param conn:
    :param post_id:
    :param user_id:
    :return:
    """
    await conn.execute(
        favorite
            .delete()
            .where(favorite.c.post_id == post_id)
            .where(favorite.c.user_id == user_id)
    )


async def db_exist_user(conn, username: str, password: str) -> ResultProxy:
    """
    Проверяет наличие пользователя в базе данных
    :param conn:
    :param username: имя пользоватея
    :param password: пароль пользователя
    :return:
    """
    result = await conn.execute(
        users
            .select()
            .where(users.c.username == username)
            .where(users.c.password == password)
    )
    return await result.fetchone()


async def db_create_user(conn, username: str, password: str):
    """
    Создаем пользователя
    :param conn:
    :param username:
    :param password:
    :return:
    """
    stmt = users.insert().values(
        username=username,
        password=password
    )
    await conn.execute(stmt)


async def db_get_all_category(conn):
    """
    Получаем все категории
    :param conn:
    :return:
    """
    return await conn.execute(category.select())


async def db_get_all_post_for_user(conn):
    """
    Получаем все посты всех пользователей
    :param conn:
    :return:
    """
    result = await conn.execute(posts.select())
    return await result.fetchall()


async def db_get_favorite_for_user(conn, user_id: int):
    """
    Получаем избранное для пользователя
    :param conn:
    :param user_id:
    :return:
    """
    j = join(posts, favorite,
             posts.c.id == favorite.c.post_id)
    stmt = select([posts]).select_from(j).where(posts.c.user_id == user_id)

    result = await conn.execute(stmt)
    return await result.fetchall()


async def db_create_post(conn, post_name: str, post_body: str, category_id: int, file_name: str, user_id: int):
    """
    Создание нового поста для пользователя
    :param conn:
    :param post_name:
    :param post_body:
    :param category_id:
    :param file_name:
    :param user_id:
    :return:
    """
    stmt = posts.insert().values(
        name=post_name,
        body=post_body,
        category_id=category_id,
        user_id=user_id,
        file_name=file_name,
    )
    await conn.execute(stmt)


async def db_update_post(conn, post_id: int, post_name: str, post_body: str, category_id: int, user_id: int):
    """
    Обновленеи поста для пользователя
    :param conn:
    :param post_id:
    :param post_name:
    :param post_body:
    :param category_id:
    :param user_id:
    :return:
    """
    stmt = posts.update().where(posts.c.id == post_id).values(
        name=post_name,
        body=post_body,
        category_id=category_id,
        user_id=user_id,
    )
    await conn.execute(stmt)


async def db_update_file_post(conn, post_id: int, file_name: str, user_id: int):
    """
    Обновляем файл для поста пользователя
    :param conn:
    :param post_id:
    :param file_name:
    :param user_id:
    :return:
    """
    stmt = posts.update().where(posts.c.id == post_id).where(posts.c.user_id == user_id).values(
        file_name=file_name
    )
    await conn.execute(stmt)


async def db_delete_user_post(conn, post_id: int, user_id: int):
    """
    Удаление пользовательского поста
    :param conn:
    :param post_id:
    :param user_id:
    :return:
    """
    await conn.execute(
        posts
            .delete()
            .where(posts.c.id == post_id)
            .where(posts.c.user_id == user_id)
    )


async def db_get_one_post(conn, post_id: int):
    """
    Получение одного поста
    :param conn:
    :param post_id:
    :return:
    """
    result = await conn.execute(
        posts
            .select()
            .where(posts.c.id == post_id)
    )
    return await result.fetchone()


async def db_get_one_user_post(conn, post_id: int, user_id: int):
    """
    Получение одного поста пользователя
    :param conn:
    :param post_id:
    :param user_id:
    :return:
    """
    result = await conn.execute(
        posts
            .select()
            .where(posts.c.id == post_id)
            .where(posts.c.user_id == user_id)
    )
    return await result.fetchone()


async def db_get_all_user_post(conn, user_id: int):
    """
    Получение всех постов для пользователя
    :param conn:
    :param user_id:
    :return:
    """
    result = await conn.execute(
        posts
            .select()
            .where(posts.c.user_id == user_id)
    )
    return await result.fetchall()


async def db_exist_post_id(conn, post_id: int, user_id: int):
    """
    Проверка на наличие поста в базе данных
    :param conn:
    :param post_id:
    :param user_id:
    :return:
    """
    result = await conn.execute(
        posts
            .select()
            .where(posts.c.id == post_id)
            .where(posts.c.user_id == user_id)
    )
    return await result.fetchone()


async def db_exist_post(conn, post_name: str, user_id: int):
    """
    Проверяем есть ли пост с идетичным именем
    :param conn:
    :param post_name:
    :param user_id:
    :return:
    """
    result = await conn.execute(
        posts
            .select()
            .where(posts.c.name == post_name)
            .where(posts.c.user_id == user_id)
    )
    return await result.fetchone()


async def db_get_category_posts(conn, category_name: str):
    """
    Получаем посты определенной категории
    :param conn:
    :param category_name:
    :return:
    """
    return await conn.execute(posts.select().where(category.c.name == category_name))


async def db_category_exist(conn, category_name: str):
    """
    Проверяем есть категория с указанным именем
    :param conn:
    :param category_name:
    :return:
    """
    result = await conn.execute(category.select().where(category.c.name == category_name))
    return await result.fetchone()


async def db_get_category_post(conn):
    """
    Получаем все категории
    :param conn:
    :return:
    """
    return await conn.execute(category.select())


async def db_create_category(conn, category_name: str):
    """
    Создаем категорию
    :param conn:
    :param category_name:
    :return:
    """
    stmt = category.insert().values(name=category_name)
    await conn.execute(stmt)


async def db_get_user_by_name(conn, username: str):
    """
    Запрос пользователя по имени
    :param conn:
    :param username:
    :return:
    """
    result = await conn.execute(users.select().where(users.c.username == username))
    return await result.fetchone()


async def db_get_user_by_id(conn, user_id: int):
    """
    Получение пользователя по id
    :param conn:
    :param user_id:
    :return:
    """
    result = await conn.execute(users.select().where(users.c.id == user_id))
    return await result.fetchone()
