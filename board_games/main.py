import jwt

from aiohttp import web
from aiopg.sa import create_engine
from cerberus import Validator
from datetime import datetime, timedelta

from settings import config
from init_db import db_init
from models import (
    db_get_user_by_id,
    db_exist_user,
    db_get_user_by_name,
    db_create_user
)
from views import (
    get_category_list,
    get_category_posts,
    get_user_post,
    post_user_post,
    put_user_post,
    delete_user_post,
    post_user_file_post,
    get_favorite_post_list,
    get_post,
    add_post_to_favorite,
    delete_post_from_favorite
)

JWT_SECRET = 'secret'
JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_SECONDS = 3000


async def login(request):
    """
    Login пользователей по логину паролю
    :param request:
    :return:
    """
    schema = {'username': {'type': 'string'}, 'password': {'type': 'string'}}
    validator = Validator(schema)
    data = await request.json()
    if not validator.validate(data):
        return web.json_response({'error': 'Validator errors'})
    username = data['username']
    password = data['password']
    async with request.app['db'].acquire() as conn:
        user = await db_exist_user(conn, username, password)
        if user is None:
            return web.json_response({"status": False, "error": "User not found"})
        payload = {
            'user_id': user.id,
            'exp': datetime.utcnow() + timedelta(seconds=JWT_EXP_DELTA_SECONDS)
        }
        jwt_token = jwt.encode(payload, JWT_SECRET, JWT_ALGORITHM)
        return web.json_response({'token': jwt_token.decode('utf-8')})


async def register_user(request):
    """
    Регистрация пользователя
    :param request:
    :return:
    """
    schema = {'username': {'type': 'string'}, 'password': {'type': 'string'}}
    validator = Validator(schema)
    data = await request.json()
    if not validator.validate(data):
        return web.json_response({'error': 'Validator errors'})
    username = data['username']
    password = data['password']
    async with request.app['db'].acquire() as conn:
        user = await db_get_user_by_name(conn, username)
        if user:
            return web.json_response({"error": "Username exist"})
        await db_create_user(conn, username, password)
        user = await db_get_user_by_name(conn, username)
        if user is None:
            return web.json_response({"status": False, "error": "User not found"})
        payload = {
            'user_id': user.id,
            'exp': datetime.utcnow() + timedelta(seconds=JWT_EXP_DELTA_SECONDS)
        }
        jwt_token = jwt.encode(payload, JWT_SECRET, JWT_ALGORITHM)
        return web.json_response({'token': jwt_token.decode('utf-8')})


@web.middleware
async def auth_middleware(request, handler):
    """
    Проверка пользователя на наличие токена
    :param app:
    :param handler:
    :return:
    """
    request.user = None
    jwt_token = request.headers.get('authorization', None)
    async with request.app['db'].acquire() as conn:
        if jwt_token:
            try:
                payload = jwt.decode(jwt_token, JWT_SECRET,
                                     algorithms=[JWT_ALGORITHM])
            except (jwt.DecodeError, jwt.ExpiredSignatureError):
                return web.json_response({'message': 'Token is invalid'}, status=400)

            request.user = await db_get_user_by_id(conn, user_id=payload['user_id'])
    return await handler(request)


@web.middleware
async def error_middleware(request, handler):
    try:
        response = await handler(request)
        if response.status != 404:
            return response
        message = response.message
    except web.HTTPException as ex:
        if ex.status != 404:
            raise
        message = ex.reason
    return web.json_response({'error': message})


async def init_pg(app: web.Application):
    """
    Создаем соединение с базой
    :param app:
    :return:
    """
    conf = app['config']['postgres']
    engine = await create_engine(
        database=conf['database'],
        user=conf['user'],
        password=conf['password'],
        host=conf['host'],
        port=conf['port'],
        minsize=conf['minsize'],
        maxsize=conf['maxsize'],
    )
    app['db'] = engine


async def close_pg(app: web.Application):
    """
    Закрываем соединение с базой
    :param app:
    :return:
    """
    app['db'].close()
    await app['db'].wait_closed()


def init_app() -> web.Application:
    app = web.Application(middlewares=[auth_middleware, error_middleware])
    app['config'] = config

    app.add_routes([
        web.post('/user/reg', register_user),
        web.post('/user/login', login),

        web.get('/category/all', get_category_list),
        web.get('/category/{name}/posts', get_category_posts),

        web.get('/post/{id}', get_post),

        web.get('/user/post/all', get_user_post),
        web.get('/user/post/{id}', get_user_post),
        web.post('/user/post/', post_user_post),
        web.put('/user/post/{id}', put_user_post),
        web.delete('/user/post/{id}', delete_user_post),
        web.post('/user/post/{id}/file', post_user_file_post),

        web.get('/user/favorite/', get_favorite_post_list),
        web.post('/user/favorite/', add_post_to_favorite),
        web.delete('/user/favorite/{id}', delete_post_from_favorite),
    ])

    return app


if __name__ == '__main__':
    db_init()
    app = init_app()

    app.on_startup.append(init_pg)
    app.on_cleanup.append(close_pg)

    web.run_app(app)
