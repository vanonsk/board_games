import time

from sqlalchemy import create_engine, MetaData

from settings import config
from models import users, category, posts, favorite

DSN = "postgresql://{user}:{password}@{host}:{port}/{database}"


def create_tables(engine):
    meta = MetaData()
    meta.create_all(bind=engine, tables=[
        users,
        category,
        posts,
        favorite
    ])


def db_init():
    while True:
        try:
            db_url = DSN.format(**config['postgres'])
            engine = create_engine(db_url)
            create_tables(engine)
        except Exception:
            time.sleep(5)
            continue
        break
