import os

from aiohttp import web
from cerberus import Validator
import imghdr
import uuid

from PIL import Image

from models import (
    db_create_category,
    db_exist_user,
    db_get_all_user_post,
    db_exist_post,
    db_create_user,
    db_get_user_by_name,
    db_get_user_by_id,
    db_get_all_category,
    db_update_file_post,
    db_delete_post_to_favorite,
    db_get_favorite_for_user,
    db_exist_post_id,
    db_delete_user_post,
    db_get_one_user_post,
    db_update_post,
    db_get_category_posts,
    db_category_exist,
    db_create_post,
    db_get_one_post,
    db_add_post_to_favorite
)

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
ORIGINAL_FILES_SERVER_PATH = os.path.join(THIS_DIR, "upload")


def login_required(func):
    def wrapper(request):
        if not request.user:
            return web.json_response({'message': 'Auth required'}, status=401)
        return func(request)
    return wrapper


@login_required
async def get_post(request):
    """
    GET запрос
    Получение конкретного поста или постов
    :param request:
    :return:
    """
    post_id = request.match_info.get('id', None)
    async with request.app['db'].acquire() as conn:
        if post_id is None:
            return web.json_response({"error": "Not found post id in query"})
        posts = await db_get_one_post(conn, post_id)
        if posts is None:
            return web.json_response({"error": "post not found"})
        result = {
            "id": posts.id,
            "name": posts.name,
            "body": posts.body,
            "file": posts.file_name,
            "category_id": posts.category_id
        }
    return web.json_response(result)


@login_required
async def get_user_post(request):
    """
    GET запрос
    Получение конкретного поста или постов для пользователя
    :param request:
    :return:
    """
    post_id = request.match_info.get('id', None)
    async with request.app['db'].acquire() as conn:
        if post_id:
            posts = await db_get_one_user_post(conn, post_id, request.user.id)
            if posts is None:
                return web.json_response({"error": "post not found"})
            result = {
                "id": posts.id,
                "name": posts.name,
                "body": posts.body,
                "file": posts.file_name,
                "category_id": posts.category_id
            }
        else:
            posts = await db_get_all_user_post(conn, request.user.id)
            result = [{
                "id": row.id,
                "name": row.name,
                "body": row.body,
                "category_id": row.category_id,
                "file": posts.file_name,
            } for row in posts]
    return web.json_response(result)


@login_required
async def post_user_file_post(request):
    """
    Загрузка файла для определенного поста пользователя
    :param request:
    :return:
    """
    post_id = request.match_info.get('id', None)
    if post_id is None:
        return web.json_response({"error": "post not found in query"})

    reader = await request.multipart()
    field = await reader.next()
    salt = str(uuid.uuid4())
    file_name = salt + "_" + field.filename
    user_file_path = os.path.join(ORIGINAL_FILES_SERVER_PATH, file_name)

    with open(user_file_path, 'wb+') as f:
        while True:
            chunk = await field.read_chunk()
            if not chunk:
                break
            f.write(chunk)
    file_format = imghdr.what(file=user_file_path)
    if file_format not in ['jpg', 'png']:
        os.unlink(user_file_path)
        return web.json_response({"success": False, "error": "Not allowed file format {}".format(file_format)})

    standart_width = 1024
    standart_height = 768

    background = Image.open(user_file_path)
    width, height = background.size
    if width > standart_width or height > standart_height:
        background = background.resize((standart_width, standart_height))
        background.save(user_file_path)
    background.close()

    # TODO стоит ли удалять старые файлы?
    # TODO сохранять ли исходные файлы?

    async with request.app['db'].acquire() as conn:
        await db_update_file_post(conn, post_id, file_name, request.user.id)
    return web.json_response({"success": True})


@login_required
async def post_user_post(request):
    """
    POST запрос
    Создание поста для пользователя
    :param request:
    :return:
    """
    schema = {'name': {'type': 'string'}, 'body': {'type': 'string'}, 'category_id': {'type': 'integer'}}
    validator = Validator(schema)
    data = await request.json()
    if not validator.validate(data):
        return web.json_response({'error': 'Validator errors'})

    async with request.app['db'].acquire() as conn:
        post = await db_exist_post(conn, data['name'], request.user.id)
        if post:
            return web.json_response({"error": "Post with this name exist. Try PUT new data"})

        await db_create_post(conn, data['name'], data['body'], data['category_id'], None, request.user.id)
    return web.json_response({"success": True})


@login_required
async def put_user_post(request):
    """
    PUT запрос
    Изменение поста пользователя
    :param request:
    :return:
    """
    post_id = request.match_info.get('id', None)
    if post_id is None:
        return web.json_response({"error": "Not found post id in query"})
    schema = {'name': {'type': 'string'}, 'body': {'type': 'string'}, 'category_id': {'type': 'integer'}}
    validator = Validator(schema)
    data = await request.json()
    if not validator.validate(data):
        return web.json_response({'error': 'Validator errors'})
    async with request.app['db'].acquire() as conn:
        post = await db_exist_post_id(conn, post_id, request.user.id)
        if post is None:
            return web.json_response({"error": "User post not exist"})
        await db_update_post(conn, post_id, data['name'], data['body'], data['category_id'], request.user.id)
        posts = await db_get_one_user_post(conn, post_id, request.user.id)
        if posts is None:
            return web.json_response({"error": "post not found"})
        result = {
            "id": posts.id,
            "name": posts.name,
            "body": posts.body,
            "category_id": posts.category_id
        }
    return web.json_response(result)


@login_required
async def delete_user_post(request):
    """
    Удаление поста пользователя
    :param request:
    :return:
    """
    post_id = request.match_info.get('id', None)
    if post_id is None:
        return web.json_response({"error": "Not found post id in query"})
    async with request.app['db'].acquire() as conn:
        post = await db_exist_post_id(conn, post_id, request.user.id)
        if post is None:
            return web.json_response({"error": "User post not exist"})
        await db_delete_user_post(conn, post_id, request.user.id)
    return web.json_response({"success": True})


@login_required
async def get_category_posts(request):
    """
    Получение постов для определенной категории
    :param request:
    :return:
    """
    category_name = request.match_info.get('name', None)
    if category_name is None:
        return web.json_response({"error": "No category name"})
    async with request.app['db'].acquire() as conn:
        category_exist = await db_category_exist(conn, category_name)
        if category_exist is None:
            return web.json_response({"error": "Category not exist"})
        db_result = await db_get_category_posts(conn, category_name)
        result = [{
            "id": row.id,
            "body": row.body,
            "category_id": row.category_id
        } for row in db_result]
    return web.json_response(result)


@login_required
async def get_category_list(request):
    """
    GET запрос
    Получение всех категории
    :param request:
    :return:
    """
    async with request.app['db'].acquire() as conn:
        all_category = await db_get_all_category(conn)
        result = [{
            "id": row.id,
            "category_name": row.name
        } for row in all_category]
    return web.json_response(result)


@login_required
async def get_favorite_post_list(request):
    """
    GET запрос
    Получение избранных постов пользователя
    :param request:
    :return:
    """
    async with request.app['db'].acquire() as conn:
        posts = await db_get_favorite_for_user(conn, request.user.id)
        result = [{
            "id": row.id,
            "name": row.name,
            "body": row.body,
            "file": row.file_name,
            "category_id": row.category_id
        } for row in posts]
    return web.json_response(result)


@login_required
async def add_post_to_favorite(request):
    """
    Добавление поста с id в избранное
    :param request:
    :return:
    """
    schema = {'post_id': {'type': 'integer'}}
    validator = Validator(schema)
    data = await request.json()
    if not validator.validate(data):
        return web.json_response({'error': 'Validator errors'})
    async with request.app['db'].acquire() as conn:
        await db_add_post_to_favorite(conn, data['post_id'], request.user.id)
    return web.json_response({"success": True})


@login_required
async def delete_post_from_favorite(request):
    """
    Удаление поста с id из избранного
    :param request:
    :return:
    """
    post_id = request.match_info.get('id', None)
    if post_id is None:
        return web.json_response({"error": "Not found post id in query"})
    async with request.app['db'].acquire() as conn:
        await db_delete_post_to_favorite(conn, post_id, request.user.id)
    return web.json_response({"success": True})

