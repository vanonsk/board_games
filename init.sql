CREATE USER board_games WITH PASSWORD 'board_games_pass';
CREATE DATABASE board_games;
GRANT ALL PRIVILEGES ON DATABASE board_games TO board_games;