FROM python:3.7-slim-stretch

WORKDIR /app

COPY board_games /app
COPY requirements.txt /app
COPY setup.py /app
COPY docker-entrypoint.sh /app

RUN pip install --trusted-host pypi.python.org -r requirements.txt

CMD ["pip3", "install", "."]
CMD ["chmod", "+x", "docker-entrypoint.sh"]
