from setuptools import setup, find_packages


setup(
    name='board_games',
    version='1.0',
    packages=find_packages('.'),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'board_games = board_games.main:main',
        ],
    },
)
